package com.zuitt.wdc044_s01.repositories;

import com.zuitt.wdc044_s01.models.Post;
import com.zuitt.wdc044_s01.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

// an interface contains behavior that a class implements
// an interface marked as @Repository contains methods for database manipulation
// by extending CrudRepository, PostRepository has inherited its pre-defined methods for creating, retrieving,
// updating, and deleting records
@Repository
public interface PostRepository extends CrudRepository<Post, Object> {
    Iterable<Post> findByUser(User user);
}
